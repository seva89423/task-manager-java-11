package ru.zorin.tm.service;

import ru.zorin.tm.api.repository.ITaskRepository;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    final private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name){
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description){
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}