package ru.zorin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();
}
