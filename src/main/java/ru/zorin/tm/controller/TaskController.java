package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.ITaskController;
import ru.zorin.tm.api.service.ITaskService;
import ru.zorin.tm.model.Task;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    public ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = taskService.findAll();
        for (Task task : tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[COMPLETE]");
    }
}