package ru.zorin.tm.controller;

import ru.zorin.tm.api.controller.IProjectController;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.model.Project;
import ru.zorin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    public IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects(){
        System.out.println("[LIST PROJECT]");
        final List<Project> projects = projectService.findAll();
        for (Project project : projects) System.out.println(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void clearProjects(){
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject(){
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[COMPLETE]");
    }
}