package ru.zorin.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_ABOUT = "about";

    String CMD_VERSION = "version";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

    String TASK_LIST = "task-list";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String PROJECT_LIST = "project-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";
}